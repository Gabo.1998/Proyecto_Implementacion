$(document).ready(function(){
	$('#formulario2').submit(function(e)
	{
		e.preventDefault();
	});
	var numMovies = 0;	
	var ventaMovie = new Array();

	$('#agregarMovie').click(function()
	{		
		var valor = $("#clienteMovie").find("option:selected").text();
		var id = $("#clienteMovie").val();						
		if (id == $('#name'+id).val())
		{
			alert("Esta categoria ya se encuentra en el pedido");					
		}
		else
		{				
			$('#column').append('<input type="text" value = "'+valor+'" disabled> <input type="hidden" value = "'+id+'" id="categoria'+id+'">');
			numMovies++;
			$("#cantidadMovie").val(numMovies);
			ventaMovie[numMovies] = $('#name'+id).val();
		}		
	});	
	//Accion Boton Envio Venta
	$('#registrarVenta').click(function(){

		var fecha = $('#fechaVenta').val();
		var nomCliente = $('#clienteMovie').val();
		var nomEmpleado = $('#empleadoMovie').val();
		var nomMovie = $('#movieVenta').val();
		var cantidadMovie = $('#cantidadGenero').val();		
		var tokenVentaMovie = $('#tokenVentaMovie').val();
		
		//Metodo Ajax para envio al controlador
		$.ajax({

			type: "post",
			url: "/venta/store",
			headers: {'X-CSRF-TOKEN': tokenVentaMovie},
			data: {fecha, nomCliente, nomEmpleado, nomMovie, cantidadMovie},
			success: function(resp){
				if(resp!="")
				{
					alert("La Venta Fue Registrada correctamente");
					window.location.href="/venta";
				}
			},
			error: function(data)
			{
				alert('Error, No se Registro la información');
			}
		});
		//fin Metodo Ajax
	});
	//Fin boton envio Movie
});