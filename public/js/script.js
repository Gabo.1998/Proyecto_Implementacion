$(document).ready(function(){
	$('#formulario').submit(function(e)
	{
		e.preventDefault();
	});
	var numCategorias = 0;	
	var generoMovie = new Array();

	$('#agregarGenero').click(function()
	{		
		var valor = $("#generoMovie").find("option:selected").text();
		var id = $("#generoMovie").val();						
		if (id == $('#categoria'+id).val())
		{
			alert("Esta categoria ya se encuentra en el pedido");					
		}
		else
		{				
			$('#column').append('<input type="text" value = "'+valor+'" disabled> <input type="hidden" value = "'+id+'" id="categoria'+id+'">');
			numCategorias++;
			$("#cantidadGenero").val(numCategorias);
			generoMovie[numCategorias] = $('#categoria'+id).val();
		}		
	});	
	//Accion Boton Envio Movie
	$('#enviarMovie').click(function(){

		var nombre = $('#nombreMovie').val();
		var descripcion = $('#descripcionMovie').val();
		var cantidadGenero = $('#cantidadGenero').val();
		var tokenMovie = $('#tokenMovie').val();
		
		//Metodo Ajax para envio al controlador
		$.ajax({

			type: "post",
			url: "/movie/store",
			headers: {'X-CSRF-TOKEN': tokenMovie},
			data: {nombre, descripcion, cantidadGenero, generoMovie},
			success: function(resp){
				if(resp!="")
				{
					alert("La Pelicula Fue Registrada correctamente");
					window.location.href="/movie";
				}
			},
			error: function(data)
			{
				alert('Error, No se Registro la información');
			}
		});
		//fin Metodo Ajax
	});
	//Fin boton envio Movie
});