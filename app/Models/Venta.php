<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model {
	protected $table = 'ventas';
	protected $filliable = ['fecha','fk_cliente','fk_empleado', 'fk_movie', 'totalVenta', 'fk_cliente', 'fk_empleado'];
	protected $guarded = ['id'];

}
