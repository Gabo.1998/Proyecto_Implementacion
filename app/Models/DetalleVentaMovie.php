<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetalleVentaMovie extends Model {

	//
	protected $table = 'detalle_venta_movies';
	protected $filliable = ['cantidad', 'fk_venta', 'fk_movie'];
	protected $guarded = ['id'];

}
