<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetalleGeneroMovie extends Model {

	protected $table = 'detalle_genero_movies';
	protected $filliable = ['fk_genero', 'fk_movie'];
	protected $guarded = ['id'];

}
