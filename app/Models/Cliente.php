<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model {

	protected $table = 'clientes';
	protected $filliable = ['identificacion','nombre', 'telefono', 'direccion', 'correo'];
	protected $guarded = ['id'];
}