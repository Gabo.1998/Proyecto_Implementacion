<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model {

	protected $table = 'empleados';
	protected $fillable = ['identificacion','nombre','telefono','correo','direccion'];
	protected $guarded = ['id'];
}
