<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController'
]);

//Entidad Genero---------------------------------------------------------------------------------

Route::resource('genero', 'GeneroController');

Route::get('genero/destroy/{id}', ['as'=>'genero/destroy', 'uses'=>'GeneroController@destroy']);

Route::post('genero/search', ['as'=>'genero/search', 'uses'=>'GeneroController@search']);

//Entidad Movie----------------------------------------------------------------------------------

Route::resource('movie', 'MovieController');

Route::get('movie/destroy/{id}', ['as'=>'movie/destroy', 'uses'=>'MovieController@destroy']);

Route::post('movie/search', ['as'=>'movie/search', 'uses'=>'MovieController@search']);
Route::post('movie/store', ['as'=>'movie/store', 'uses'=>'MovieController@store']);

//Entidad Empleado-------------------------------------------------------------------------------

Route::resource('empleado', 'EmpleadoController');

Route::get('empleado/destroy/{id}', ['as'=>'empleado/destroy', 'uses'=>'EmpleadoController@destroy']);

Route::post('empleado/search', ['as'=>'empleado/search', 'uses'=>'EmpleadoController@search']);

//Entidad Cliente-------------------------------------------------------------------------------

Route::resource('cliente', 'ClienteController');

Route::get('cliente/destroy/{Id}', ['as'=>'cliente/destroy', 'uses'=>'ClienteController@destroy']);

Route::post('cliente/search', ['as'=>'cliente/search', 'uses'=>'ClienteController@search']);

//Entidad Venta---------------------------------------------------------------------------------

Route::resource('venta', 'VentaController');

Route::get('venta/destroy/{Id}', ['as'=>'venta/destroy', 'uses'=>'VentaController@destroy']);

Route::post('venta/search', ['as'=>'venta/search', 'uses'=>'VentaController@search']);

Route::post('venta/store', ['as'=>'venta/store', 'uses'=>'VentaController@store']);




