<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Cliente as Cliente;
use Illuminate\Http\Request;

class ClienteController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$clientes = Cliente::all();
		return \View::make('Cliente/list', compact('clientes'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return \View::make('Cliente/new');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$cliente = new Cliente;
		$cliente->identificacion = $request->identificacion;
		$cliente->nombre = $request->nombre;
		$cliente->telefono = $request->telefono;
		$cliente->correo = $request->correo;
		$cliente->direccion = $request->direccion;
		$cliente->save();
		return redirect('cliente');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$cliente = Cliente::find($id);
		return \View::make('Cliente/update', compact('cliente'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{		
		$cliente = CLiente::find($request->id);
		$cliente->nombre = $request->nombre;
		$cliente->telefono = $request->telefono;
		$cliente->correo = $request->correo;
		$cliente->direccion = $request->direccion;
		$cliente->save();
		return redirect('cliente');
	}

	public function search(Request $request)
	{
		$clientes = Cliente::where('nombre','like','%'.$request->name.'%')->get();
		return \View::make('Cliente/list', compact('clientes'));
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$cliente = Cliente::find($id);
		$cliente->delete();
		return redirect()->back();
	}

}