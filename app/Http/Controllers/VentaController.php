<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Venta as Venta;
use App\Models\Movie as Movie;
use App\Models\Cliente as Cliente;
use App\Models\Empleado as Empleado;
use Illuminate\Http\Request;

class VentaController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$movies = Movie::select('id','name')->get();
		$clientes = Cliente::select('id','nombre')->get();
		$empleados = Empleado::select('id','nombre')->get();

		return \View::make('Venta/venta', compact('clientes','empleados','movies'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if($request->ajax())
		{
			$venta = new Venta;
			$venta->fecha = $request->fecha;
			$venta->nomCliente = $request->nomCliente;
			$venta->nomEmpleado = $request->nomEmpleado;
			$venta->nomMovie = $request->nomMovie;
			$venta->cantidadMovie = $request->cantidadMovie;			
			$venta->save();

			//Registro en la tabla detalle por fluent
			$idVenta = $venta->id;

			for($i=1; $i <= $request->cantidadMovie ;$i++)
			{
				\DB::table('detalle_movie_venta')->insertGetId(
					['fk_movie' => $request->movieVenta[$i], 'fk_venta' => $idVenta ]);
			} 
			//fin Registro


			return redirect('venta');
		}	
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
}
