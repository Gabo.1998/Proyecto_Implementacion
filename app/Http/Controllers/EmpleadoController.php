<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Empleado as Empleado;
use Illuminate\Http\Request;

class EmpleadoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$empleados = Empleado::all();
		return \View::make('Empleado/list', compact('empleados'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return \View::make('Empleado/new');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$empleado = new Empleado;
		$empleado->identificacion = $request->identificacion;
		$empleado->nombre = $request->nombre;
		$empleado->telefono = $request->telefono;
		$empleado->correo = $request->correo;
		$empleado->direccion = $request->direccion;
		$empleado->save();
		return redirect('empleado');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$empleado = Empleado::find($id);
		return \View::make('Empleado/update', compact('empleado'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$empleado = Empleado::find($request->id);
		$empleado->nombre = $request->nombre;
		$empleado->telefono = $request->telefono;
		$empleado->direccion = $request->direccion;
		$empleado->correo = $request->correo;
		$empleado->save();
		return redirect('empleado');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	//consultar
	public function search(Request $request)
	{
		$empleados = Empleado::where('nombre','like','%'.$request->name.'%')->get();
		return \View::make('Empleado/list', compact('empleados'));
	}
	//Borrar
	public function destroy($id)
	{
		$empleado = Empleado::find($id);
		$empleado->delete();
		return redirect()->back();
	}

}
