<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Genero as Genero;
use App\Models\Movie as Movie;
use Illuminate\Http\Request;

class MovieController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$movies = Movie::all();
		
		return \View::make('Movie/list', compact('movies'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$generos = Genero::select('id','nombre')->get();
		return \View::make('Movie/new', compact('generos'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if($request->ajax())
		{
			$movie = new Movie;
			$movie->name = $request->nombre;
			$movie->description = $request->descripcion;	
			$movie->save();

			//Registro en la tabla detalle por fluent
			$idMovie = $movie->id;

			for($i=1; $i <= $request->cantidadGenero ;$i++)
			{
				\DB::table('detalle_genero_movies')->insertGetId(
					['fk_genero' => $request->generoMovie[$i], 'fk_movie' => $idMovie ]);
			} 
			//fin Registro


			return redirect('movie');
		}		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$movie = Movie::find($id);
		return \View::make('Movie/update', compact('movie'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$movie = Movie::find($request->id);
		$movie->name = $request->name;
		$movie->description = $request->description;
		$movie->save();
		return redirect('movie');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	
	//Consultar
	public function search(Request $request)
	{
		$movies = Movie::where('name','like','%'.$request->name.'%')->get();
		return \View::make('Movie/list', compact('movies'));
	}

	//Borrar
	public function destroy($id)
	{
		$movie = Movie::find($id);
		$movie->delete();
		return redirect()->back();
	}

}
