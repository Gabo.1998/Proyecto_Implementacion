<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Genero as Genero;
use App\Models\Movie as Movie;
use Illuminate\Http\Request;

class GeneroController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$generos = Genero::all();
		
		return \View::make('Genero/list', compact('generos'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return \View::make('Genero/new');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$genero = new Genero;
		$genero->nombre = $request->nombre;
		$genero->descripcion = $request->descripcion;
		$genero->save();
		return redirect('genero');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$genero = Genero::find($id);
		return \View::make('Genero/update', compact('genero'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$genero = Movie::find($request->id);
		$genero->nombre = $request->nombre;
		$genero->descripcion = $request->descripcion;
		$genero->save();
		return redirect('genero');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	//Consultar
	public function search(Request $request)
	{
		$generos = Genero::where('nombre','like','%'.$request->name.'%')->get();

		return \View::make('Genero/list', compact('generos'));
	}

	public function destroy($id)
	{
		$genero = Genero::find($id);
		$genero->delete();
		return \View::make('Genero/list', compact('generos'));
	}

}
