<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Laravel</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('/style.css') }}" rel="stylesheet">
	<script src="{{ asset('js/jquery-1.12.2.min.js') }}"></script>
		<script src="{{ asset('js/script.js') }}"></script>
		<script src="{{ asset('js/script2.js') }}"></script>

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<link async href="http://fonts.googleapis.com/css?family=Fredoka%20One" data-generated="http://enjoycss.com" rel="stylesheet" type="text/css"/>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav>
		<ul class="menu">
		    <li><a href="{{ url('/') }}"><span class=""><i class="icon icon-home"></i></span>Home</a></li>
		    <li><a href="{{ url('/genero') }}"><span class=""><i class="icon icon-ticket"></i></span>Generos</a>
		    <ul>
		    	<li><a href="#">Item #1</a></li>
		    	<li><a href="#">Item #2</a></li>
		    	<li><a href="#">Item #3</a></li>
		    	<li><a href="#">Item #4</a></li>
		    </ul>
		    </li>

		    <li><a href="{{ url('/movie') }}"><span class=""><i class="icon icon-film"></i></span>Movies</a></li>
			<li><a href="{{ url('/empleado') }}"><span class=""><i class="icon icon-user-tie"></i></span>Empleados</a></li>
		    <li><a href="{{ url('/cliente') }}"><span class=""><i class="icon icon-man-woman"></i></span>Clientes</a></li>
			<li><a href="{{ url('/venta') }}"><span class=""><i class="icon icon-cart"></i></span>Ventas</a></li>
			<li><a href="{{ url('/auth/list') }}"><span class=""><i class="icon icon-users"></i></span>Usuarios</a></li>

					@if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}"><span class=""><i class="icon icon-users"></i></span>Login</a></li>
						<li><a href="{{ url('/auth/register') }}"><span class=""><i class="icon icon-users"></i></span>Register</a></li>
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
		</ul>
	</nav>

	<section class="contenedor">
	    <div class="init">
		   <center><h1 class="swing">Movi☢teca</h1></center>
        </div>
	</section>

	@yield('content')

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>
