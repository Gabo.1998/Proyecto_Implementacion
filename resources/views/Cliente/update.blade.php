@extends('app');
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			{!! Form::model($cliente, ['route' => 'cliente.update', 'method' => 'put', 'novalidate'])!!}
			{!! Form::hidden('id', $cliente->id)!!}
			<div class="form-group">
				{!! Form::label('lb_nombre', 'Nombre')!!}
				{!! Form::text('nombre', null, ['class' => 'form-control','required' => 'required'])!!}
			</div>
			<div class="form-group">
				{!! Form::label('lb_telefono', 'Telefono')!!}
				{!! Form::text('telefono', null, ['class' => 'form-control','required' => 'required'])!!}
			</div>
			<div class="form-group">
				{!! Form::label('lb_direccion', 'Direccion')!!}
				{!! Form::text('direccion', null, ['class' => 'form-control','required' => 'required'])!!}
			</div>
			<div class="form-group">
				{!! Form::label('lb_correo', 'Correo')!!}
				{!! Form::text('correo', null, ['class' => 'form-control','required' => 'required'])!!}
			</div>
			<div class="form-group">
				{!! Form::submit('Enviar', ['class' => 'btn btn-success'])!!}
			</div>
			{!! Form::close()!!}
		</div>
	</div>
</div>
@endsection