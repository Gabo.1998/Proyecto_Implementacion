@extends('app')
@section('content')
<div class="container">
	<div class="row">
		{!! Form::open(['route'=>'cliente/search', 'method'=>'post', 'novalidate', 'class'=>'form-inline'])!!}
		<div class="form-group">
			<label for="exampleInputName2">Nombre</label>
			<input type="text" class="form-control" name="name">
		</div>
		<button type="submit" class="btn btn-default">Buscar</button>
		<a href="{{ route('cliente.index') }}" class="btn btn-primary">Todos</a>
		<a href="{{ route('cliente.create') }}" class="btn btn-primary">Registrar</a>
		{!! Form::close() !!}
		<br>
		<table class="table table-condensed table-striped table-bordered">
			<thead>
				<tr>
					<th>Identificacion</th>
					<th>Nombre</th>
					<th>Telefono</th>
					<th>Direccion</th>
					<th>Correo</th>
					<th>Accion</th>
				</tr>
			</thead>
			<tbody>
				@foreach($clientes as $cliente)
				<tr>
					<td>{{ $cliente->identificacion }}</td>
					<td>{{ $cliente->nombre }}</td>
					<td>{{ $cliente->telefono}}</td>
					<td>{{ $cliente->direccion}}</td>
					<td>{{ $cliente->correo}}</td>
					<td>
						<a class="btn btn-primary btn-xs" href="{{ route('cliente.edit',['id'=>$cliente->id])}}">Editar</a>
						<a class="btn btn-danger btn-xs" href="{{ route('cliente/destroy',['id'=>$cliente->id]) }}">Borrar</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection