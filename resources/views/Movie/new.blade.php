@extends('app')
@section('content')
<div class="container">
	<div class="row">
		<div class"col-md-10 col-md-offset-1">
			{!! Form::open(['id' => 'formulario','route' => 'movie.store', 'method' => 'post', 'novalidate']) !!}
				<input type="hidden" name="tokenMovie" id="tokenMovie" value="{{ csrf_token() }}">
				<div class="Form-group">
					{!! Form::label('full_name', 'Nombre')!!}
					{!! Form::text('name', null, ['id' =>'nombreMovie', 'class' => 'form-control', 'required' => 'required'])!!}
				</div>
				<div class="Form-group">
					{!! Form::label('email', 'Descripcion')!!}
					{!! Form::text('description', null, ['id' =>'descripcionMovie','class'=>'form-control', 'required' => 'required'])!!}
				</div>
				<div class="Form-group" >
					{!! Form::label('lb_genero', 'Seleccionar genero')!!}
					<select class="form-control" id="generoMovie">
						@foreach($generos as $genero)
							<option value="{{$genero->id}}">{{$genero->nombre}}</option>
						@endforeach
					</select>
				<div class="Form-group">
					{!! Form::button('Agregar', ['id' => 'agregarGenero','class'=>'btn btn-success']) !!}
			    </div>
				</div>			
				<div id="column">
					{!! Form::hidden('cantidad', 0, ['id'=>'cantidadGenero']) !!}
				</div>
				<div class="Form-group">
					{!! Form::submit('Enviar', ['id'=> 'enviarMovie', 'class'=>'btn btn-success'])!!}				
				</div>			
			{!! Form::close()!!}
			
		</div>
	</div>
</div>
@endsection