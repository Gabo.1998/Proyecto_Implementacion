@extends('app')
@section('content')
<div class="container">
	<div class="row">
		{!! Form::open(['route'=>'movie/search', 'method'=>'post', 'novalidate', 'class'=>'form-inline'])!!}
		<div class="form-group">
			<label for="exampleInputName2">Nombre</label>
			<input type="text" class="form-control" name="name">
		</div>
		<button type="submit" class="btn btn-default">Buscar</button>
		<a href="{{ route('movie.index') }}" class="btn btn-primary">Todos</a>
		<a href="{{ route('movie.create') }}" class="btn btn-primary">Registrar</a>
		{!! Form::close() !!}
		<br>
		<table class="table table-condensed table-striped table-bordered">
			<thead>
				<tr>
				    <td>Id</td>
					<td>Nombre</td>
					<td>Descripcion</td>
					<td>Generos</td>
					<td>Valor</td>
					<td>Accion</td>
				</tr>
			</thead>
			<tbody>
				@foreach($movies as $movie)
				<tr>
				    <td>{{ $movie->id}}</td>
					<td>{{ $movie->name }} </td>
					<td>{{ $movie->description}} </td>
					<td>{{ $movie->generoMovie}} </td>
					<td>{{ $movie->valorAlquiler}} </td>
					<td>
						<a class="btn btn-primary" href="{{ route('movie.edit',['id'=>$movie->id])}}">Editar</a>
						<a class="btn btn-danger" href="{{ route('movie/destroy', ['id'=>$movie->id]) }}">Borrar</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection