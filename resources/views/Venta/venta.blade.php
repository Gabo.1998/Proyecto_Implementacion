@extends('app')
@section('content')
<div class="container">
	<div class="row">
		<div class"col-md-10 col-md-offset-1">
			{!! Form::open(['id' => 'formulario2', 'route'=>'venta.store', 'method'=>'post', 'novalidate', 'class'=>'form-inline'])!!}
			<input type="hidden" name="tokenMovie" id="tokenMovie" value="{{ csrf_token() }}">
	<h4 class="vent">Ventas</h4>
			<label for="exampleInputName2">Fecha</label>
			<input id="fechaVenta" type="date" name="fecha" step="1" min="2016-01-01" max="2016-12-31" value="2016-03-25">
		
			{!! Form::label('lb_cliente', 'Seleccionar Cliente')!!}
			<select class="form-control" id="clienteMovie">
				@foreach($clientes as $cliente)
					<option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
				@endforeach
			</select>
		
		
			{!! Form::label('Lb_empleado', 'Seleccionar Empleado')!!}
			<select class="form-control" id="empleadoMovie">
				@foreach($empleados as $empleado)
					<option value="{{$empleado->id}}">{{$empleado->nombre}}</option>
				@endforeach
			</select>
			<br>
		</div>

		<table class="table table-condensed table-striped table-bordered">
			<thead>
				<tr>
					<th>Movie</th>
					<th>Cantidad</th>
					<th>Accion</th>
				</tr>
				<tr>
					<th>
						<select class="form-control" id="movieVenta" name="movieVenta">
							@foreach($movies as $movie)
								<option value="{{$movie->id}}">{{$movie->name}}</option>
							@endforeach
						</select>
					</th>
					<th id="colunm">
						{!! Form::number('cantidad', null, ['id'=>'cantidadGenero', 'class'=>'form-control', 'required' => 'required'])!!}
					</th>
					<th>
					{!! Form::button('Añadir al carrito', ['id' => 'agregarMovie','class'=>'btn btn-primary']) !!}
			            
					</th>
				</tr>
			</thead>
		</table>

		<br>
		<h4 class="vent">Carrito</h4>
		<br>
		<table class="table table-condensed table-striped table-bordered">
			<thead>
				<tr>
				    <th>Id</th>
					<th>Nombre</th>
					<th>Descripcion</th>
					<th>Cantidad</th>
					<th>Valor Unitario</th>
					<th>Valor Total</th>
					<th>Acción</th>					
				</tr>
			</thead>
			<tbody>
				@foreach($movies as $movie)
				<tr>
				    <td>{{ $movie->id }}</td>
					<td>{{ $movie->name }} </td>
					<td>{{ $movie->description }} </td>
					<td></td>
					<td></td>
					<td></td>
					<td>
						<a class="btn btn-primary" href="{{ route('movie.edit',['id'=>$movie->id])}}">Editar</a>
						<a class="btn btn-danger" href="{{ route('movie/destroy', ['id'=>$movie->id]) }}">Borrar</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		<button type="submit" class="btn btn-success" id="registrarVenta">Registrar venta</button>
		{!! Form::close() !!}

	</div>
</div>

@endsection