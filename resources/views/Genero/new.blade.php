@extends('app')
@section('content')
<div class="container">
	<div class="row">
		<div class"col-md-10 col-md-offset-1">
			{!! Form::open(['route' => 'genero.store', 'method' => 'post', 'novalidate']) !!}
			<div class="Form-group">
				{!! Form::label('full_name', 'Nombre')!!}
				{!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required'])!!}
			</div>
			<div class="Form-group">
				{!! Form::label('email', 'Descripcion')!!}
				{!! Form::text('descripcion', null, ['class'=>'form-control', 'required' => 'required'])!!}
			</div>
			<div class="Form-group">
				{!! Form::submit('Enviar', ['class'=>'btn btn-success'])!!}
			</div>
			<div>
				{!! Form::close()!!}
			</div>
		</div>
	</div>
</div>
@endsection