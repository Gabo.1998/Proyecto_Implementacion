@extends('app')
@section('content')
<div class="container">
	<div class="row">
		{!! Form::open(['route'=>'genero/search', 'method'=>'post', 'novalidate', 'class'=>'form-inline'])!!}
		<div class="form-group">
			<label for="exampleInputName2">Nombre</label>
			<input type="text" class="form-control" name="name">
		</div>
		<button type="submit" class="btn btn-default">Buscar</button>
		<a href="{{ route('genero.index') }}" class="btn btn-primary">Todos</a>
		<a href="{{ route('genero.create') }}" class="btn btn-primary">Registrar</a>
		{!! Form::close() !!}
		<br>
		<table class="table table-condensed table-striped table-bordered">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Descripcion</th>
					<th>Accion</th>
				</tr>
			</thead>
			<tbody>
				@foreach($generos as $genero)
				<tr>
					<td>{{ $genero->nombre }}</td>
					<td>{{ $genero->descripcion }}</td>
					<td>
						<a class="btn btn-primary btn-xs" href="{{ route('genero.edit',['id'=>$genero->id])}}">Editar</a>
						<a class="btn btn-danger btn-xs" href="{{ route('genero/destroy', ['id'=>$genero->id]) }}">Borrar</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection