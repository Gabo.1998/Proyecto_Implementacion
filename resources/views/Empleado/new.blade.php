@extends('app')
@section('content')
<div class="container">
	<div class="row">
		<div class"col-md-10 col-md-offset-1">
			{!! Form::open(['route' => 'empleado.store', 'method' => 'post', 'novalidate']) !!}
			<div class="Form-group">
				{!! Form::label('lb_id', 'Identificacion')!!}
				{!! Form::text('identificacion', null, ['class'=>'form-control', 'required' => 'required'])!!}
			</div>
			<div class="Form-group">
				{!! Form::label('lb_nombre', 'Nombre')!!}
				{!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required'])!!}
			</div>
			<div class="Form-group">
				{!! Form::label('lb_telefono', 'Telefono')!!}
				{!! Form::text('telefono', null, ['class'=>'form-control', 'required' => 'required'])!!}
			</div>
			<div class="Form-group">
				{!! Form::label('Lb_direccion', 'Direccion')!!}
				{!! Form::text('direccion', null, ['class'=>'form-control', 'required' => 'required'])!!}
			</div>
			<div class="Form-group">
				{!! Form::label('lb_correo', 'Correo')!!}
				{!! Form::text('correo', null, ['class'=>'form-control', 'required' => 'required'])!!}
			</div>
			<div class="Form-group">
				{!! Form::submit('Enviar', ['class'=>'btn btn-success'])!!}
			</div>
			<div>
				{!! Form::close()!!}
			</div>
		</div>
	</div>
</div>
@endsection