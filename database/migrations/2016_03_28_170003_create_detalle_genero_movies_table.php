<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleGeneroMoviesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('detalle_genero_movies', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('fk_genero')->unsigned();
			$table->integer('fk_movie')->unsigned();

			$table->foreign('fk_genero')->references('id')->on('generos')->onUpdate('cascade');
			$table->foreign('fk_movie')->references('id')->on('movies')->onUpdate('cascade');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('detalle_genero_movies');
	}

}
