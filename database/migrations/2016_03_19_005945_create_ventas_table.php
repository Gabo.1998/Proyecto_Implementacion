<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ventas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->date('fecha');
			$table->string('nomCliente');
			$table->string('nomEmpleado');
			$table->string('nomMovie');
			$table->string('cantidadMovie');
			$table->integer('totalVenta');

			$table->integer('fk_cliente')->unsigned();
			$table->integer('fk_empleado')->unsigned();

			$table->foreign('fk_cliente')->references('id')->on('clientes')->onUpdate('cascade');
			$table->foreign('fk_empleado')->references('id')->on('empleados')->onUpdate('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ventas');
	}

}
