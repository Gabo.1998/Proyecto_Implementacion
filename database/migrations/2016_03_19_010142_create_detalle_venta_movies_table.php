<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleVentaMoviesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('detalle_venta_movies', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('cantidad');
			$table->integer('fk_venta')->unsigned();
			$table->integer('fk_movie')->unsigned();

			$table->foreign('fk_venta')->references('id')->on('ventas')->onUpdate('cascade');
			$table->foreign('fk_movie')->references('id')->on('movies')->onUpdate('cascade');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('detalle_venta_movies');
	}

}
